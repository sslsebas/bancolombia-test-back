const mongoose = require("mongoose")
const schema = mongoose.Schema;

const CoinSchema = new schema({
    symbol: {
        type: String
    },
    name: {
        type: String
    },
    value: {
        type: Number
    }
}, {
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model("Coins", CoinSchema)