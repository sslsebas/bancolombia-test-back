const mongoose = require("mongoose")
const schema = mongoose.Schema;

const CountrySchema = new schema({
    name: {
        type: String
    },
    avaibleCoins: [{
        type: schema.Types.ObjectId,
        ref: "Coins" 
    }],
    managers: {
        type: Array
    }
}, {
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model("Countries", CountrySchema)