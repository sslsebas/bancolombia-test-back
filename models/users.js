const mongoose = require("mongoose")
const schema = mongoose.Schema

const UserSchema = new schema({
    name: {
        type: String
    },
    country: {
        type: schema.Types.ObjectId,
        ref: "Countries"
    }
}, {
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model("Users", UserSchema)