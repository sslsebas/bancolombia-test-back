const user = require("../models/users")

const userController = {
    createUser: async(req, res) => {
        const params = req.body;
        user.create(params).then( ( createdUser ) => {
            res.send(createdUser)
        }, error =>{
            res.send({...error, error:true})
        });
    },

    getUsers: async(req, res) => {
        const Users = await user.find();
        if ( Users ) {
            res.send(Users)
        } else {
            res.send({message: "Users not found"})
        }
    }

}

module.exports = userController