const coin = require("../models/coins")

const coinsController = {
    createCoins: async(req, res) => {
        const params = req.body;
        coin.create(params).then( ( createdCoin ) => {
            res.send(createdCoin)
        }, error =>{
            res.send({...error, error:true})
        });
    },

    getCoins: async(req, res) => {
        const coins = await coin.find();
        if ( coins ) {
            res.send(coins)
        } else {
            res.send({message: "coins not found"})
        }
    },

    deleteCoin: async(req, res) => {
        const body = req.body;
        try {
            const { _id } = body;
            if (_id) {
                coin.findOneAndDelete({_id}, {},
                    (err,doc, response) => {
                        if(err){
                            res.status(500).send({...error, error:true})
                        }else{
                            res.send(doc)
                        }
                    }
                )
            }
        } catch(error) {
            res.send({message: "coins not found"})
        }
    },

    updateCoin: async(req, res) => {
        const params = req.body;
        try {
            const { _id, fields } = params;
            if (_id) {
                coin.findOneAndUpdate({_id}, { $set : fields },
                    (err,doc, response) => {
                        if(err){
                            res.status(500).send({...error, error:true})
                        }else{
                            res.send(doc)
                        }
                    }
                )
            }
        } catch(error) {
            res.send({message: "coins not found"})
        }
    }


}

module.exports = coinsController