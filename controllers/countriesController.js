const country = require("../models/countries")

const countryController = {
    createCountry: async(req, res) => {
        const params = req.body;
        country.create(params).then( ( createdCountry ) => {
            res.send(createdCountry)
        }, error =>{
            res.send({...error, error:true})
        });
    },

    getCountries: async(req, res) => {
        const countries = await country.find();
        if ( countries ) {
            res.send(countries)
        } else {
            res.send({message: "countries not found"})
        }
    }

}

module.exports = countryController