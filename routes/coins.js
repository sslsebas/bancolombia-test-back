const express = require("express")
const router = express.Router()
// const coinsController = require("../controllers/coinsController")
const {getCoins, createCoins, deleteCoin, updateCoin} = require("../controllers/coinsController")

router.get("/", getCoins)
router.post("/", createCoins) 
router.post("/delete", deleteCoin) 
router.post("/update", updateCoin) 


module.exports = router