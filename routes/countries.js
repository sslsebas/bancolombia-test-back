const express = require("express")
const router = express.Router()
const { getCountries, createCountry } = require("../controllers/countriesController")

router.get("/", getCountries)
router.post("/", createCountry) 


// router.get("/:id", getItem)

module.exports = router